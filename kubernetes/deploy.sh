#!/bin/bash

echo "Production deploy"

#[[ -d .kube ]] || mkdir .kube
#[[ -d .kube/config ]] || mkdir .kube/config

#Copia la configuracion del cluster a usar
cp ./kubernetes/frapp-kubeconfig.yaml ~/.kube/config

#Crea un namespace
#kubectl apply -f ./kubernetes/namespace.yaml

#Lanza los deployments de cada contenedor
kubectl apply -f ./kubernetes/controlManager-deployment.yaml
kubectl apply -f ./kubernetes/controlServer-deployment.yaml
kubectl apply -f ./kubernetes/controlClient-deployment.yaml

#Crea los services de cada contenedor
kubectl apply -f ./kubernetes/controlManager-service_np.yaml
kubectl apply -f ./kubernetes/controlServer-service_np.yaml
kubectl apply -f ./kubernetes/controlClient-service_np.yaml

#Crea loadBalancer de cada contenedor
kubectl apply -f ./kubernetes/controlManager-lb.yaml
kubectl apply -f ./kubernetes/controlServer-lb.yaml
kubectl apply -f ./kubernetes/controlClient-lb.yaml