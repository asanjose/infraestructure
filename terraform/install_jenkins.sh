

#!/bin/bash
sudo apt-get -y update

echo "Install Java JDK 8"
apt-get remove -y java
apt-get install -y java-1.8.0-openjdk

echo "Install Maven"
apt-get install -y maven 

echo "Install git"
apt-get install -y git

echo "Install Docker engine"
apt-get update -y
apt-get install docker -y
#sudo usermod -a -G docker jenkins
#sudo service docker start
sudo chkconfig docker on

echo "Install Jenkins"
wget -O /etc/apt-get.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
apt-get install -y jenkins
sudo usermod -a -G docker jenkins
sudo chkconfig jenkins on
sudo service docker start
sudo service jenkins start

echo "Install Kubectl"
apt-get install -y kubectl

